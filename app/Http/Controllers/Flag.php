<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Flag extends Controller
{
    
    public static function serve( $pair, $query ){
        
        $q_s = explode('+', $query);

        if( strstr($query, ':') ){

            $res = 'green';
            
            foreach( $q_s as $q ){
                
                list($tf, $defined_perc) = explode(':', $q);

                if( self::tf_fluc($pair, $tf) >= $defined_perc ){
                    $res = 'red';
                    // break;
                }

            }

            return response()->json([
                'status' => 'OK',
                'res' => $res,
            ]);
        
        } else {

            $res = [];

            foreach( $q_s as $tf )
                $res[ $tf ] = self::tf_fluc($pair, $tf);

            return response()->json([
                'status' => 'ER',
                'res' => $res,
            ]);

        }

    }


    private static function tf_fluc( $pair, $tf ){
        
        list($rep_tf, $rep_bts) = self::tf_n_bts_by_tf($tf);

        $json = self::ta("symbol={$pair}&interval={$rep_tf}&backtracks={$rep_bts}");
        $arr = json_decode($json);

        foreach( $arr as $item ){
            
            if( !isset($the_max) or $item->high > $the_max )
                $the_max = $item->high;

            if( !isset($the_min) or $item->low < $the_min )
                $the_min = $item->low;

        }

        $diff = $the_max - $the_min;
        $perc = round( ($diff / $the_min) * 100, 2);
        
        return $perc;
        
    }


    private static function tf_n_bts_by_tf( $tf ){

        $arr = [
            '1m' => [ '1m',  2  ],
            '3m' => [ '1m',  4  ],
            '5m' => [ '1m',  6  ],
            '15m'=> [ '1m',  16 ],
            '30m'=> [ '3m',  11 ],
            '45m'=> [ '3m',  16 ],
            '1h' => [ '5m',  13 ],
            '2h' => [ '15m', 9  ],
            '3h' => [ '15m', 13 ],
            '4h' => [ '15m', 17 ],
            '1d' => [ '4h',  7  ],
            '1w' => [ '1d',  8  ],
            '1M' => [ '1w',  5  ],
            '3M' => [ '1w',  13 ],
            '6M' => [ '1M',  7  ],
            '12M'=> [ '1M',  13 ],
        ];

        return $arr[ $tf ];

    }


    private static function ta($params){
        return self::taapi('candle', 'exchange=binance&'.$params);
    }


    # path: 'candle', 'exchange=binance&symbol=btcusdt&interval=1d&backtracks=8'
    private static function taapi( $indicator, $params, $looped=false ){

        if(! $secret = env('TAAPI_SECRET') )
            die('TAAPI_SECRET not defined in .env');

        $url = 'https://api.taapi.io/'.$indicator.'/?'.$params.'&secret='.$secret;
        $res = @file_get_contents($url , false, stream_context_create([ "ssl"=> ["verify_peer"=>false, "verify_peer_name"=>false ]]));

        if( $res ){
            return $res;

        } else if(! $looped ){
            sleep(15);
            return self::taapi($indicator, $params, true);
        }

    }


}

